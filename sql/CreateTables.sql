CREATE DATABASE IF NOT EXISTS myquiz;
USE myquiz;

-- Create User table
DROP TABLE IF EXISTS User;
CREATE TABLE IF NOT EXISTS User (
	user_id INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(42),
    lastname VARCHAR(42),
	email VARCHAR(42) UNIQUE,
    password VARCHAR(42),
    is_active BOOL,
    is_admin BOOL
);
INSERT INTO User (firstname, lastname, email, password, is_active, is_admin) VALUES 
('Jimmy', 'Chen', 'jimmy@gmail.com', '123', true, true),
('Tester', 'One', 'test1@gmail.com', '123', true, false),
('Tester', 'Two', 'test2@gmail.com', '123', false, false);
SELECT * FROM User;

-- Create Category table
DROP TABLE IF EXISTS Category;
CREATE TABLE IF NOT EXISTS Category (
	category_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(42)
);
INSERT INTO Category (name) VALUES 
('Math'),
('Technology'),
('NBA');
SELECT * FROM Category;

-- Create Quiz table
DROP TABLE IF EXISTS Quiz;
CREATE TABLE IF NOT EXISTS Quiz (
	quiz_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(42),
    take_date DATE,
    time_start TIMESTAMP,
    time_end TIMESTAMP,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES User(user_id),
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES Category(category_id)
);
-- INSERT INTO Quiz (name, take_date, time_start, time_end, user_id, category_id) VALUES 
-- ('quiz1', '2023-06-10', '2023-06-10 10:00:00', '2023-06-10 11:00:00', 2, 1),
-- ('quiz2', '2023-06-07', '2023-06-07 11:00:00', '2023-06-07 12:00:00', 2, 2),
-- ('quiz3', '2023-06-09', '2023-06-09 12:00:00', '2023-06-09 13:00:00', 2, 3),
-- ('quiz3', '2023-06-09', '2023-06-09 12:00:00', '2023-06-09 13:00:00', 3, 3);
SELECT * FROM Quiz;

-- Create Question table
DROP TABLE IF EXISTS Question;
CREATE TABLE IF NOT EXISTS Question (
	question_id INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(100),
    status BOOL,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES Category(category_id)
);
INSERT INTO Question (description, status, category_id) VALUES 
('1 + 1 = ', true, 1),
('10 * 2 = ', true, 1),
('6 / 3 = ', true, 1),
('0 + 100 = ', true, 1),
('3 * 7 = ', true, 1),
('2 + 2 + 2 = ', true, 1),
('81 - 5 = ', true, 1),
('6 + 5 = ', true, 1),
('77 / 11 = ', true, 1),
('123 + 321 = ', true, 1),
('Which one is not a product of Apple?', true, 2),
('In which year was Java first released?', true, 2),
('Which one is not an OOP principle?', true, 2),
('Who is the CEO of Tesla?', true, 2),
('What does "AR" stands for?', true, 2),
('What does "SQL" stands for?', true, 2),
('Which company is not part of FAANG?', true, 2),
('How many GB is equal to 4096MB?', true, 2),
('Which programming language is a functional language?', true, 2),
('What is the value of N if 2^N equals 1024?', true, 2),
('What does "NBA" stands for?', true, 3),
('How many teams are in NBA now?', true, 3),
('Which team won the 2022 NBA Championship?', true, 3),
('Which player is used in the NBA logo?', true, 3),
('Who holds the record for the most championships in NBA history?', true, 3),
('Who holds the record for the most three-pointers made in NBA history?', true, 3),
('How many NBA teams are based in New York City?', true, 3),
('How many fouls can a player receive in a single game?', true, 3),
('How long does an NBA game last?', true, 3),
('Which team is not one of the NBA teams?', true, 3);
SELECT * FROM Question;

-- Create Choice table
DROP TABLE IF EXISTS Choice;
CREATE TABLE IF NOT EXISTS Choice (
	choice_id INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(100),
    is_correct BOOL,
    question_id INT,
    FOREIGN KEY (question_id) REFERENCES Question(question_id)
);
INSERT INTO Choice (description, is_correct, question_id) VALUES 
('0', false, 1),
('1', false, 1),
('2', true, 1),
('3', false, 1),
('10', false, 2),
('20', true, 2),
('30', false, 2),
('40', false, 2),
('2', true, 3),
('4', false, 3),
('6', false, 3),
('8', false, 3),
('-100', false, 4),
('0', false, 4),
('100', true, 4),
('1000', false, 4),
('21', true, 5),
('37', false, 5),
('10', false, 5),
('4', false, 5),
('0', false, 6),
('2', false, 6),
('4', false, 6),
('6', true, 6),
('56', false, 7),
('66', false, 7),
('76', true, 7),
('86', false, 7),
('1', false, 8),
('11', true, 8),
('21', false, 8),
('31', false, 8),
('6', false, 9),
('7', true, 9),
('8', false, 9),
('9', false, 9),
('123321', false, 10),
('321123', false, 10),
('444', true, 10),
('333', false, 10),
('iPhone', false, 11),
('iMac', false, 11),
('iWatch', true, 11),
('iPad', false, 11),
('1965', false, 12),
('1975', false, 12),
('1985', false, 12),
('1995', true, 12),
('Abstraction', false, 13),
('Polymorphism', false, 13),
('Inheritance', false, 13),
('Compiling', true, 13),
('Elon Musk', true, 14),
('Tim Cook', false, 14),
('Sundar Pichai', false, 14),
('Satya Nadella', false, 14),
('All Related', false, 15),
('Austin Reaves', false, 15),
('Augmented Reality', true, 15),
('Anything Round', false, 15),
('Structured Query Language', true, 16),
('Suspicious Questionable Language', false, 16),
('Supreme Query Language', false, 16),
('Se-Quential Language', false, 16),
('Apple', false, 17),
('Nvidia', true, 17),
('Google', false, 17),
('Amazon', false, 17),
('1', false, 18),
('2', false, 18),
('3', false, 18),
('4', true, 18),
('Java', false, 19),
('Scheme', true, 19),
('C++', false, 19),
('Python', false, 19),
('6', false, 20),
('8', false, 20),
('10', true, 20),
('12', false, 20),
('Natural Beauty Association', false, 21),
('Neutral Balanced Atmosphere', false, 21),
('National Basketball Association', true, 21),
('National Brisket Association', false, 21),
('28', false, 22),
('29', false, 22),
('30', true, 22),
('31', false, 22),
('Boston Celtics', false, 23),
('Golden State Warriors', true, 23),
('Miami Heats', false, 23),
('Mil Bucks', false, 23),
('LeBron James', false, 24),
('Michael Jordan', false, 24),
('Kobe Bryant', false, 24),
('Jerry West', true, 24),
('Bill Russell', true, 25),
('Michael Jordan', false, 25),
('Sam Jones', false, 25),
('Kareem Abdul-Jabbar', false, 25),
('Ray Allen', false, 26),
('Reggie Miller', false, 26),
('Stephan Curry', true, 26),
('James Harden', false, 26),
('0', false, 27),
('1', false, 27),
('2', true, 27),
('3', false, 27),
('4', false, 28),
('5', false, 28),
('6', true, 28),
('7', false, 28),
('40', false, 29),
('48', true, 29),
('56', false, 29),
('60', false, 29),
('Golden State Warriors', false, 30),
('Los Angeles Lakers', false, 30),
('New York Knicks', false, 30),
('Houston Astros', true, 30);
SELECT * FROM Choice;

-- Create QuestionResponse table
DROP TABLE IF EXISTS QuestionResponse;
CREATE TABLE IF NOT EXISTS QuestionResponse (
	qr_id INT AUTO_INCREMENT PRIMARY KEY,
    question_id INT,
    FOREIGN KEY (question_id) REFERENCES Question(question_id),
    quiz_id INT,
    FOREIGN KEY (quiz_id) REFERENCES Quiz(quiz_id),
    user_choice_id INT,
    FOREIGN KEY (user_choice_id) REFERENCES Choice(choice_id)
);
-- INSERT INTO QuestionResponse (question_id, quiz_id, user_choice_id) VALUES 
-- (1, 1, 1),
-- (2, 1, 5),
-- (3, 1, 9),
-- (4, 1, 16),
-- (5, 1, 20),
-- (6, 2, 22),
-- (7, 2, 26),
-- (8, 2, 30),
-- (9, 2, 33),
-- (10, 2, 38),
-- (11, 3, 42),
-- (12, 3, 45),
-- (13, 3, 49),
-- (14, 3, 53),
-- (15, 3, 60),
-- (11, 4, 42),
-- (12, 4, 45),
-- (13, 4, 49),
-- (14, 4, 53),
-- (15, 4, 60);
SELECT * FROM QuestionResponse;

-- Create Message table
DROP TABLE IF EXISTS Message;
CREATE TABLE IF NOT EXISTS Message (
	message_id INT AUTO_INCREMENT PRIMARY KEY,
    subject VARCHAR(42),
    email VARCHAR(42),
    content VARCHAR(200),
    create_time TIMESTAMP
);
-- INSERT INTO Message (subject, email, content, create_time) VALUES 
-- ('question one', 'test1@gmail.com', 'can you give me more instruction', '2023-06-09 10:00:00');
SELECT * FROM Message;
