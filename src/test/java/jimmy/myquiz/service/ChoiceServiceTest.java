package jimmy.myquiz.service;

import jimmy.myquiz.dao.ChoiceDao;
import jimmy.myquiz.domain.Choice;
import jimmy.myquiz.exception.ChoiceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ChoiceServiceTest {
    @Mock
    private ChoiceDao choiceDao;

    @InjectMocks
    private ChoiceService choiceService;

    private Choice choice;

    @BeforeEach
    public void setup() {
        choice = Choice.builder()
                .choice_id(1)
                .description("0")
                .question_id(1)
                .is_correct(false)
                .build();
    }

    @DisplayName("JUnit test for getChoice method - success")
    @Test
    void test_getChoice_success() throws ChoiceNotFoundException {
        Choice expected = new Choice(1, "1", false, 1);
        Mockito.when(choiceDao.getChoice(1)).thenReturn(expected);
        assertEquals(expected, choiceService.getChoiceByChoiceID(1));
    }

    @DisplayName("JUnit test for getChoice method - failed")
    @Test
    void test_getChoice_failed() {
        Mockito.when(choiceDao.getChoice(-1)).thenReturn(null);
        assertThrows(ChoiceNotFoundException.class, () -> choiceService.getChoiceByChoiceID(-1));
    }

    @DisplayName("JUnit test for getChoicesByQuestionID method")
    @Test
    void test_getChoicesByQuestionID_success() {
        List<Choice> expected = new ArrayList<>();
        expected.add(new Choice(1, "1", false, 1));
        expected.add(new Choice(2, "2", true, 1));
        expected.add(new Choice(3, "3", false, 1));
        Mockito.when(choiceDao.getChoicesByQuestionID(1)).thenReturn(expected);
        assertEquals(expected, choiceService.getChoicesByQuestionID(1));
    }

    @DisplayName("JUnit test for updateChoiceDescription method")
    @Test
    void test_updateChoiceDescription_success() {
        Choice expected = new Choice(1, "1", false, 1);
        expected.setDescription("updated");
        expected.set_correct(true);
        choiceService.updateChoiceDescription(1, "updated");
        choiceService.updateChoiceCorrectness(1, true);
        assertEquals(expected.getDescription(), "updated");
        assertEquals(expected.getIs_correct(), true);
    }

    @DisplayName("JUnit test for addChoice method")
    @Test
    void test_getChoicesByQuestionID_successWhenEmpty() {
        int expectedRowsUpdated = 1;
        Mockito.when(choiceDao.addChoice(choice.getDescription(), choice.getIs_correct(), choice.getQuestion_id())).thenReturn(expectedRowsUpdated);
        int actualRowsUpdated = choiceDao.addChoice(choice.getDescription(), choice.getIs_correct(), choice.getQuestion_id());
        assertEquals(expectedRowsUpdated, actualRowsUpdated);
        Mockito.verify(choiceDao).addChoice(choice.getDescription(), choice.getIs_correct(), choice.getQuestion_id());
    }
}
