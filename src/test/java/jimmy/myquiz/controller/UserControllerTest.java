package jimmy.myquiz.controller;

import jimmy.myquiz.domain.User;
import jimmy.myquiz.service.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @DisplayName("test getUsers() function")
    @Test
    void test_getUsers() throws Exception {
        List<User> expected = new ArrayList<>();
        expected.add(new User(1, "first1", "last1", "test1@email", "pw1", true, false));
        expected.add(new User(2, "first2", "last2", "test2@email", "pw2", true, false));
        expected.add(new User(3, "first3", "last3", "test3@email", "pw3", true, false));
        Mockito.when(userService.getUsersNotAdmin()).thenReturn(expected);
        HashMap<String, Object> sessionattr = new HashMap<String, Object>();
        sessionattr.put("admin", Boolean.TRUE);
        mockMvc.perform(MockMvcRequestBuilders.get("/users")
                        .sessionAttrs(sessionattr))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].user_id", Matchers.containsInAnyOrder(1, 2, 3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].firstname", Matchers.containsInAnyOrder("first1", "first2", "first3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].lastname", Matchers.containsInAnyOrder("last1", "last2", "last3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].email", Matchers.containsInAnyOrder("test1@email", "test2@email", "test3@email")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].password", Matchers.containsInAnyOrder("pw1", "pw2", "pw3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].is_active", Matchers.containsInAnyOrder(true, true, true)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*]._admin", Matchers.containsInAnyOrder(false, false, false)));
    }

//    @DisplayName("test updateUser() function")
//    @Test
//    void test_updateUser() throws Exception {
//        User expected = new User(1, "first1", "last1", "test1@email", "pw1", true, false);
//        Mockito.when(userService.updateUserStatus(Mockito.isA(Integer.class), Mockito.isA(Boolean.class))).thenReturn(1);
//
//        Gson gson = new Gson();
//        String expectedJson = gson.toJson(expected);
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/user-update")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(expectedJson))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.content().json("1"));
//
//    }
}
