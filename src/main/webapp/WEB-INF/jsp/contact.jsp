<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Contact Us</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Contact</h1>
    </div>
    <form method="post" action="/contact">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="subject" name="subject" required>
            <label for="subject">Subject</label>
        </div>
        <div class="form-floating mb-3">
            <input type="email" class="form-control" id="email" name="email" required>
            <label for="email">Email Address</label>
        </div>
        <div class="form-floating mb-3">
            <textarea type="text" class="form-control" id="content" name="content" rows="10" cols="50"></textarea>
            <label for="content">Message Content</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>

</html>
