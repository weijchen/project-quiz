<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Add Question</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Add Question</h1>
    </div>
    <div class="mt-4">
        <form method="post" action="/question/add">
            <div>
                <h3>Question Description</h3>
                <div class="form-floating mb-3">
                <textarea type="text" class="form-control" id="description" name="description" rows="5" cols="40"
                          required></textarea>
                    <label for="description">Description</label>
                </div>
            </div>
            <div>
                <h4>Category ID</h4>
                <c:forEach items="${categories}" var="category" varStatus="loop">
                    <p>${category.name} - ${category.category_id}</p>
                </c:forEach>

                <div class="form-floating mb-3">
                    <input type="number" class="form-control" id="category" name="category" required/>
                    <label for="category">Please select Category ID</label>
                </div>
            </div>
            <div>
                <h3>Question Choices</h3>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="descOne" name="descOne" required/>
                    <label for="descOne">Choices One</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="descTwo" name="descTwo" required/>
                    <label for="descTwo">Choices One</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="descThree" name="descThree" required/>
                    <label for="descThree">Choices One</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="descFour" name="descFour" required/>
                    <label for="descFour">Choices One</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="number" class="form-control" id="answer" name="answer" required/>
                    <label for="answer">Answer? (1-4):</label>
                </div>
            </div>

            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>
    <a href="/questions">
        <button name="button" type="button" class="btn btn-success">Back to Questions Management</button>
    </a>
</div>
</body>

</html>
