<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<html>
<head>
    <title>Contacts Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Contacts Management</h1>
    </div>
    <div class="mt-4">
        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Subject</th>
                <th scope="col">Email Address</th>
                <th scope="col">Time</th>
                <th scope="col">Message Content</th>
                <th scope="col">View</th>
            </tr>
            </thead>
            <c:forEach items="${messages}" var="message" varStatus="loop">
                <tr>
                    <td>${message.subject}</td>
                    <td>${message.email}</td>
                    <td>${message.create_time}</td>
                    <c:choose>
                        <c:when test="${fn:length(message.content) > 5}">
                            <c:set var = "minMessage" value = "${fn:substring(message.content, 0, 5)}" />
                            <td>${minMessage}...</td>
                        </c:when>
                        <c:otherwise>
                            <td>${message.content}</td>
                        </c:otherwise>
                    </c:choose>
                    <c:url value="/contacts/view" var="viewContacts">
                        <c:param name="cid" value="${message.message_id}"/>
                    </c:url>
                    <td>
                        <a class="btn btn-sm btn-primary" href="${viewContacts}">View</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
