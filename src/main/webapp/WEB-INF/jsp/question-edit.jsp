<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Edit Question</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Edit Question</h1>
    </div>
    <div class="mt-4">
        <div>
            <h2>${question.description}</h2>
            <c:url value="/question/edit/question" var="editQuestion">
                <c:param name="question_id" value="${question.question_id}"/>
                <c:param name="description"/>
            </c:url>
            <form action="${editQuestion}" method="post">
                <div class="input-group mb-3">
                    <input class="form-control" type="text" value="${question.description}" name="description">
                    <input class="btn btn-primary" type="submit" value="Update Description"/>
                </div>
            </form>
            <h4>Category - ${category}</h4>
        </div>
        <div class="mt-4">
            <h2>Choices</h2>
            <c:forEach items="${choices}" var="choice" varStatus="loop">
                <div class="mt-4">
                    <h4>${choice.description}</h4>

                    <div class="mt-4">
                        <c:url value="/question/edit/choice-description" var="editChoiceDescription">
                            <c:param name="question_id" value="${question.question_id}"/>
                            <c:param name="description"/>
                            <c:param name="choice_id" value="${choice.choice_id}"/>
                        </c:url>
                        <form action="${editChoiceDescription}" method="post">
                            <div class="input-group mb-3">
                                <input class="form-control" type="text" value="${choice.description}"
                                       name="description">
                                <input class="btn btn-primary" type="submit" value="Update Choice"/>
                            </div>
                        </form>
                    </div>

                    <div class="d-flex">
                        <div class="p-2">
                            <p>Answer: <strong>${choice.is_correct}</strong></p>
                        </div>
                        <div class="p-2">
                            <c:url value="/question/edit/choice-answer" var="editChoiceAnswer">
                                <c:param name="question_id" value="${question.question_id}"/>
                                <c:param name="choice_id" value="${choice.choice_id}"/>
                            </c:url>

                            <form action="${editChoiceAnswer}" method="post">
                                <input class="btn btn-sm btn-success" type="submit" value="Mark as Answer"/>
                            </form>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <a href="/questions">
        <button name="button" type="button" class="btn btn-success">Back</button>
    </a>
</div>
</body>

</html>
