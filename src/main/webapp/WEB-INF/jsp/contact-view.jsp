<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Contacts Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Message Details</h1>
    </div>
    <div class="mt-4">
        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Subject</th>
                <th scope="col">Email Address</th>
                <th scope="col">Time</th>
            </tr>
            </thead>
            <tr>
                <td>${viewMessage.subject}</td>
                <td>${viewMessage.email}</td>
                <td>${viewMessage.create_time}</td>
            </tr>
        </table>
        <h2>Message Content</h2>
        <p>
            ${viewMessage.content}
        </p>
        <a class="btn btn-sm btn-success" href="/contacts">Back</a>
    </div>
</div>
</body>
</html>
