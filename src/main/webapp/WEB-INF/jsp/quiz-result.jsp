<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Quiz Result</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Quiz Result</h1>
    </div>
    <div>
        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Quiz Name</th>
                <th scope="col">User Name</th>
                <th scope="col">Start Time</th>
                <th scope="col">End Time</th>
                <th scope="col">Correct</th>
                <th scope="col">Total</th>
                <th scope="col">Result</th>
            </tr>
            </thead>
            <tr>
                <td>${quizResult.name}</td>
                <td>${fullName}</td>
                <td>${quizResult.timeStart}</td>
                <td>${quizResult.timeEnd}</td>
                <td>${quizResult.passQuestion}</td>
                <td>${quizResult.totalQuestion}</td>
                <c:choose>
                    <c:when test="${quizResult.passQuestion >= 3}">
                        <th scope="col" style="color: darkcyan">Passed</th>
                    </c:when>
                    <c:otherwise>
                        <th scope="col" style="color: red">Failed</th>
                    </c:otherwise>
                </c:choose>
            </tr>
        </table>
    </div>
    <div>
        <c:forEach begin="0" end="${qsize-1}" var="i" varStatus="loop">
            <h2>Q${i+1} - ${questions[i].description}</h2>
            <table class="table">
                <thead>
                <tr class="table-light">
                    <th scope="col">Description</th>
                    <th scope="col">Selection</th>
                </tr>
                </thead>
                <c:forEach items="${choices[i]}" var="choice" varStatus="loop">
                    <tr>
                        <c:choose>
                            <c:when test="${choice.is_correct}">
                                <td style="color: darkcyan; vertical-align: middle"><strong>${choice.description}</strong></td>
                            </c:when>
                            <c:otherwise>
                                <td style="vertical-align: middle">${choice.description}</td>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${choice.choice_id == selections[i]}">
                                <td style="vertical-align: middle; color: rosybrown">Your Selection</td>
                            </c:when>
                            <c:otherwise>
                                <td></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
            </table>
        </c:forEach>
    </div>
    <c:choose>
        <c:when test="${sessionScope.get('admin')}">
            <a href="/quizzes">
                <button name="button" type="button" class="btn btn-success">Back</button>
            </a>
        </c:when>
        <c:otherwise>
            <a href="/home">
                <button name="button" type="button" class="btn btn-success">Take Another Quiz</button>
            </a>
        </c:otherwise>
    </c:choose>
</div>
</body>

</html>
