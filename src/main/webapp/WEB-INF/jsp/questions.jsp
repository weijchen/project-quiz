<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Questions Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Questions Management</h1>
        <a href="/question/add">
            <button name="button" type="button" class="btn btn-success">Add Question</button>
        </a>
    </div>
    <div class="mt-4">
        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Edit</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <c:if test="${numQuestion != 0}">
                <c:forEach begin="0" end="${numQuestion-1}" var="i" varStatus="loop">
                    <tr>
                        <td>${categories[i]}</td>
                        <td>${questions[i].description}</td>

                        <c:url value="/question/edit" var="editQuestionUrl">
                            <c:param name="question_id" value="${questions[i].question_id}"/>
                        </c:url>
                        <td>
                            <a href="${editQuestionUrl}">Edit</a>
                        </td>
                        <c:choose>
                            <c:when test="${questions[i].status}">
                                <td>Active</td>
                            </c:when>
                            <c:otherwise>
                                <td>Inactive</td>
                            </c:otherwise>
                        </c:choose>
                        <td>
                            <div style="height: 36px">
                                <c:url value="/question-update" var="updateQuestionStatus">
                                    <c:param name="question_id" value="${questions[i].question_id}"/>
                                    <c:param name="toStatus" value="${true}"/>
                                </c:url>
                                <c:choose>
                                    <c:when test="${questions[i].status}">
                                        <input class="btn btn-sm btn-secondary" type="submit" value="Activate"
                                               disabled/>
                                    </c:when>
                                    <c:otherwise>
                                        <form action="${updateQuestionStatus}" method="post">
                                            <input class="btn btn-sm btn-primary" type="submit" value="Activate"/>
                                        </form>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div style="height: 36px">
                                <c:url value="/question-update" var="updateQuestionStatus">
                                    <c:param name="question_id" value="${questions[i].question_id}"/>
                                    <c:param name="toStatus" value="${false}"/>
                                </c:url>
                                <c:choose>
                                    <c:when test="${questions[i].status}">
                                        <form action="${updateQuestionStatus}" method="post">
                                            <input class="btn btn-sm btn-primary" type="submit" value="Deactivate"/>
                                        </form>
                                    </c:when>
                                    <c:otherwise>
                                        <input class="btn btn-sm btn-secondary" type="submit" value="Deactivate"
                                               disabled/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
    </div>
</div>
</body>

</html>
