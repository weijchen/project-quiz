<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <c:choose>
            <c:when test="${sessionScope.get('admin')}">
                <div class="mt-4">
                    <h1>Hello Admin, which function to manage?</h1>
                </div>
                <div class="d-flex flex-row mb-3">
                    <a href="/users" class="p-2">
                        <button name="button" type="button" class="btn btn-warning">Users</button>
                    </a>
                    <a href="/quizzes" class="p-2">
                        <button name="button" type="button" class="btn btn-warning">Quizzes Result</button>
                    </a>
                    <a href="/questions" class="p-2">
                        <button name="button" type="button" class="btn btn-warning">Questions</button>
                    </a>
                    <a href="/contacts" class="p-2">
                        <button name="button" type="button" class="btn btn-warning">Contacts</button>
                    </a>
                </div>
            </c:when>
            <c:otherwise>
                <div>
                    <div class="mt-2">
                        <h1>Home</h1>
                    </div>
                    <div class="d-flex justify-content-around">
                        <c:forEach items="${categories}" var="category" varStatus="loop">
                            <div class="p-2">
                                <h2>${category.name}</h2>
                                <c:url value="/quiz" var="takeTestUrl">
                                    <c:param name="cid" value="${category.category_id}"/>
                                </c:url>
                                <c:choose>
                                    <c:when test="${sessionScope.get('ongoing')}">
                                        <p style="color: red">Test Ongoing</p>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${takeTestUrl}">
                                            <button name="button" type="button" class="btn btn-primary">Take Test
                                            </button>
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="d-flex justify-content-center">
                        <c:if test="${sessionScope.get('ongoing')}">
                            <a href="/quiz">
                                <button name="button" type="button" class="btn btn-success">Resume Test</button>
                            </a>
                        </c:if>
                    </div>
                    <div class="mt-4 d-flex justify-content-start">
                        <table class="table">
                            <thead>
                            <tr class="table-light">
                                <th scope="col">Name</th>
                                <th scope="col">Date Taken</th>
                                <th scope="col">URL</th>
                            </tr>
                            </thead>
                            <c:forEach items="${quizzes}" var="quiz" varStatus="loop">
                                <tr>
                                    <td>${quiz.name}</td>
                                    <td>${quiz.take_date}</td>
                                    <c:url value="/quiz-result" var="quizResultUrl">
                                        <c:param name="q" value="${quiz.quiz_id}"/>
                                        <c:param name="u" value="${quiz.user_id}"/>
                                    </c:url>
                                    <td>
                                        <a href="${quizResultUrl}">Quiz Result</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>

</html>
