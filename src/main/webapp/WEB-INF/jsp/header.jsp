<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container mt-3">
    <nav class="navbar navbar-expand-lg bg-body-tertiary" data-bs-theme="dark">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <c:choose>
                        <c:when test="${sessionScope.get('admin')}">
                            <a class="navbar-brand" href="/home">Quizzz</a>
                            <a class="nav-link" href="/home">Home</a>
                            <a class="nav-link" href="/users">Users</a>
                            <a class="nav-link" href="/quizzes">Quizzes Result</a>
                            <a class="nav-link" href="/questions">Questions</a>
                            <a class="nav-link" href="/contacts">Contacts</a>
                            <a class="nav-link" href="/logout">Logout</a>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${sessionScope != null && sessionScope.get('email') != null}">
                                    <a class="nav-link" href="/home">Home</a>
                                    <a class="nav-link" href="/contact">Contact Us</a>
                                    <c:if test="${sessionScope.get('ongoing')}">
                                        <a class="nav-link" href="/quiz">Resume Test</a>
                                    </c:if>
                                    <a class="nav-link" href="/logout">Logout</a>
                                </c:when>
                                <c:otherwise>
                                    <a class="nav-link" href="/login">Login</a>
                                    <a class="nav-link" href="/register">Register</a>
                                    <a class="nav-link" href="/contact">Contact Us</a>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </nav>
</div>
<%--session.getAttribute("user") != null--%>