<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Users Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Users Management</h1>
    </div>
    <div class="mt-4">
        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <c:forEach items="${users}" var="user" varStatus="loop">
                <tr>
                    <td>${user.firstname} ${user.lastname}</td>
                    <td>${user.email}</td>
                    <td>
                        <c:choose>
                            <c:when test="${user.is_active}">Active</c:when>
                            <c:otherwise>Inactive</c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <div style="height: 36px">
                            <c:url value="/user-update" var="updateUserStatus">
                                <c:param name="user_id" value="${user.user_id}"/>
                                <c:param name="toStatus" value="${true}"/>
                            </c:url>
                            <c:choose>
                                <c:when test="${user.is_active}">
                                    <input class="btn btn-sm btn-secondary" type="submit" value="Activate" disabled/>
                                </c:when>
                                <c:otherwise>
                                    <form action="${updateUserStatus}" method="post">
                                        <input class="btn btn-sm btn-primary" type="submit" value="Activate"/>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div style="height: 36px">
                            <c:url value="/user-update" var="updateUserStatus">
                                <c:param name="user_id" value="${user.user_id}"/>
                                <c:param name="toStatus" value="${false}"/>
                            </c:url>
                            <c:choose>
                                <c:when test="${user.is_active}">
                                    <form action="${updateUserStatus}" method="post">
                                        <input class="btn btn-sm btn-primary" type="submit" value="Deactivate"/>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <input class="btn btn-sm btn-secondary" type="submit" value="Deactivate" disabled/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>

</html>
