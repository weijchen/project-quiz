<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Quiz</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Quiz</h1>
    </div>
    <c:url value="/submit-quiz" var="submitAns">
    </c:url>
    <form action="${submitAns}" method="post">
        <c:choose>
            <c:when test="${hasFinish}">
                <input class="btn btn-success" type="submit" value="Submit"/>
            </c:when>
            <c:otherwise>
                <input class="btn btn-secondary" type="submit" value="Submit" disabled/>
            </c:otherwise>
        </c:choose>
    </form>
    <div class="mt-4">
        <div class="d-flex justify-content-center" style="align-items: baseline">
            <div class="p-4 m-4">
                <c:choose>
                    <c:when test="${sessionScope.get('cur') == 1}">
                        <input class="btn btn-warning" type="submit" value="Previous" disabled/>
                    </c:when>
                    <c:otherwise>
                        <form action="/quiz/update-cur" method="post">
                            <input type="hidden" name="cur" value="${sessionScope.get('cur')-1}"/>
                            <input class="btn btn-warning" type="submit" value="Previous"/>
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>

            <c:forEach begin="1" end="${qsize}" var="i" varStatus="loop">
                <c:choose>
                    <c:when test="${sessionScope.get('cur') == i}">
                        <input class="btn btn-success p-2 m-2" style="height: min-content;" type="button"
                               value="Q${i}"/>
                    </c:when>
                    <c:otherwise>
                        <input class="btn btn-success p-2 m-2" style="height: min-content;" type="button" value="Q${i}"
                               disabled/>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <div class="p-4 m-4">
                <c:choose>
                    <c:when test="${sessionScope.get('cur') == sessionScope.get('qsize')}">
                        <input class="btn btn-warning" type="submit" value="Next" disabled/>
                    </c:when>
                    <c:otherwise>
                        <form action="/quiz/update-cur" method="post">
                            <input type="hidden" name="cur" value="${sessionScope.get('cur')+1}"/>
                            <input class="btn btn-warning" type="submit" value="Next"/>
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <h2>Q${sessionScope.get('cur')} - ${ongoingQuestions[sessionScope.get('cur')-1].description}</h2>
        <c:forEach items="${choices[sessionScope.get('cur')-1]}" var="choice" varStatus="loop">
            <div class="d-flex" style="height: 48px">
                <div class="p-2">
                    <p>${choice.description}</p>
                </div>
                <div class="p-2">
                    <c:choose>
                        <c:when test="${choice.choice_id == selections[sessionScope.get('cur')-1]}">
                            <input class="btn btn-sm btn-secondary" type="button" value="Selected"
                                   name="question${sessionScope.get('cur')-1}"
                                   disabled>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/quiz" var="selectAns">
                                <c:param name="question_id"
                                         value="${ongoingQuestions[sessionScope.get('cur')-1].question_id}"/>
                                <c:param name="user_choice_id" value="${choice.choice_id}"/>
                                <c:param name="cur" value="${sessionScope.get('cur')}"/>
                            </c:url>

                            <form action="${selectAns}" method="post">
                                <input class="btn btn-sm btn-primary" type="submit" value="Select Answer"/>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </c:forEach>
        <c:if test="${selections[sessionScope.get('cur')-1] != -1}">
            <p style="color: red">Answered</p>
        </c:if>
    </div>
</div>
</body>

</html>
