<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Login</h1>
    </div>
    <div class="mt-4">
        <form method="post" action="/login">
            <div class="form-floating mb-3">
                <input type="email" class="form-control" id="email" placeholder="name@example.com"
                       name="email">
                <label for="email">Email address</label>
            </div>
            <div class="form-floating mb-3">
                <input type="password" class="form-control" id="password" name="password">
                <label for="password">Password</label>
            </div>
            <p style="color: red">${sessionScope.get('errorMsg')}</p>
            <div class="row">
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="col-md-1">
                    <a href="/register">
                        <button name="button" value="OK" type="button" class="btn btn-success">Register</button>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

</html>
