<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Quizzes Result Management</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="mt-2">
        <h1>Quizzes Result Management</h1>
    </div>
    <div class="mt-4">
        <form method="get" action="/quizzes">
            <div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="category" name="category"
                           value="${sessionScope.get("searchCategory")}">
                    <label for="category">Category</label>
                </div>
            </div>
            <div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="userInfo" name="userInfo"
                           value="${sessionScope.get("searchUserInfo")}">
                    <label for="userInfo">User Info (Name or Email):</label>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Filter</button>
        </form>

        <table class="table">
            <thead>
            <tr class="table-light">
                <th scope="col">Taken Time</th>
                <th scope="col">Category</th>
                <th scope="col">User Full Name</th>
                <th scope="col">No. of question</th>
                <th scope="col">Score</th>
                <th scope="col">Details</th>
            </tr>
            </thead>
            <c:if test="${numQuizzes != 0}">
                <c:forEach begin="0" end="${numQuizzes-1}" var="i" varStatus="loop">
                    <tr>
                        <td>${quizzes[i].take_date}</td>
                        <td>${categories[i]}</td>
                        <td>${users[i]}</td>
                        <td>${total[i]}</td>
                        <td>${passed[i]} / ${total[i]}</td>
                        <c:url value="/quiz-result" var="quizResultUrl">
                            <c:param name="q" value="${quizzes[i].quiz_id}"/>
                            <c:param name="u" value="${quizzes[i].user_id}"/>
                        </c:url>
                        <td>
                            <a href="${quizResultUrl}">Details</a>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
    </div>
</div>
</body>

</html>
