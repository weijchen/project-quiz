package jimmy.myquiz.service;

import jimmy.myquiz.dao.UserDao;
import jimmy.myquiz.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public void createUser(String firstname, String lastname, String email, String password, boolean is_admin) {
        userDao.createUser(firstname, lastname, email, password, is_admin);
    }

    public boolean isUserExist(String email) {
        return userDao.isUserExist(email);
    }

    public boolean isUserExist(int user_id) {
        return userDao.isUserExist(user_id);
    }

    public Optional<User> validateLogin(String email, String password) {
        return userDao.getUsers().stream()
                .filter(u -> u.getEmail().equals(email)
                        && u.getPassword().equals(password)
                        && u.getIs_active()
                )
                .findAny();
    }

    public String getUserFullName(int user_id) {
        User user = userDao.getUser(user_id);
        return user.getFirstname() + " " + user.getLastname();
    }

    public int updateUserStatus(int user_id, boolean toStatus) {
        if (isUserExist(user_id)) {
            return userDao.updateUserStatus(user_id, toStatus);
        }
        return 0;

    }

    public List<User> getUsersNotAdmin() {
        List<User> users = userDao.getUsers();
        List<User> normalUsers = users.stream().filter(u -> !u.is_admin()).collect(Collectors.toList());
        return normalUsers;
    }
}
