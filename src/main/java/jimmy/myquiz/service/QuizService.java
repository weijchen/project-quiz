package jimmy.myquiz.service;

import jimmy.myquiz.dao.*;
import jimmy.myquiz.domain.QuestionResponse;
import jimmy.myquiz.domain.Quiz;
import jimmy.myquiz.responsemodel.QuizResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuizService {
    private final QuizDao quizDao;
    private final QuestionResponseDao questionResponseDao;
    private final ChoiceDao choiceDao;
    private final CategoryDao categoryDao;

    @Autowired
    public QuizService(QuizDao quizDao, QuestionResponseDao questionResponseDao, QuestionDao questionDao, ChoiceDao choiceDao, CategoryDao categoryDao) {
        this.quizDao = quizDao;
        this.questionResponseDao = questionResponseDao;
        this.choiceDao = choiceDao;
        this.categoryDao = categoryDao;
    }

    public List<Quiz> getQuizzes() {
        return quizDao.getQuizzes();
    }

    public List<Quiz> getQuizzes(String category, String userInfo) {
        List<Quiz> quizzes;
        // filter by user info
        if (userInfo.equals("")) {
            quizzes = getQuizzes();
        } else {
            quizzes = quizDao.getQuizzes(userInfo);
        }

        // filter by category
        if (!category.equals("")) {
            return quizzes
                    .stream()
                    .filter(quiz -> categoryDao.getCategoryName(quiz.getCategory_id()).startsWith(category))
                    .collect(Collectors.toList());
        }
        return quizzes;
    }

    public List<Quiz> getQuizzesByUserId(int user_id) {
        return quizDao.getQuizzesByUserId(user_id);
    }

    public QuizResult getQuizResult(int quiz_id) {
        Quiz quiz = quizDao.getQuizByQuizId(quiz_id);
        String name = quiz.getName();
        Date date = quiz.getTake_date();
        Timestamp time_start = quiz.getTime_start();
        Timestamp time_end = quiz.getTime_end();
        int user_id = quiz.getUser_id();

        List<QuestionResponse> responses = questionResponseDao.getQuestionsResponsesByQuizId(quiz_id);

        QuizResult quizResult = new QuizResult(name, date, time_start, time_end, user_id, responses, responses.size());
        for (QuestionResponse response : responses) {
            if (choiceDao.getChoice(response.getUser_choice_id()).getIs_correct()) {
                quizResult.increment();
            }
        }

        return quizResult;
    }

    public int createQuiz(int category_id, String name, int user_id) {
        String catName = categoryDao.getCategoryName(category_id);
        return quizDao.createQuiz(catName + " " + name, user_id, category_id);
    }

    public void updateQuiz(int quiz_id, LocalDateTime time_end) {
        quizDao.updateQuizEndTime(quiz_id, time_end);
    }
}
