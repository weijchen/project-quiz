package jimmy.myquiz.service;

import jimmy.myquiz.dao.QuestionResponseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionResponseService {
    private final QuestionResponseDao questionResponseDao;

    @Autowired
    public QuestionResponseService(QuestionResponseDao questionResponseDao) {
        this.questionResponseDao = questionResponseDao;
    }

    public int getQuestionResponse(int question_id, int quiz_id) {
        if (questionResponseDao.getQuestionResponse(question_id, quiz_id) == null) {
            return -1;
        } else {
            return questionResponseDao.getQuestionResponse(question_id, quiz_id).getUser_choice_id();
        }
    }

//    public List<Integer> getQuestionsByQuizId(int quiz_id) {
//        List<QuestionResponse> responses = questionResponseDao.getQuestionsResponsesByQuizId(quiz_id);
//        List<Integer> questionsId = new ArrayList<>();
//        for (QuestionResponse response : responses) {
//            questionsId.add(response.getQuestion_id());
//        }
//        return questionsId;
//    }
//

    public void updateQuestionResponse(int question_id, int quiz_id, int user_choice_id) {
        if (getQuestionResponse(question_id, quiz_id) == -1) {
            questionResponseDao.createQuestionResponse(question_id, quiz_id, user_choice_id);
        } else {
            questionResponseDao.updateQuestionResponse(question_id, quiz_id, user_choice_id);
        }
    }
}
