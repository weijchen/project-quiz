package jimmy.myquiz.service;

import jimmy.myquiz.dao.ChoiceDao;
import jimmy.myquiz.domain.Choice;
import jimmy.myquiz.exception.ChoiceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChoiceService {
    private final ChoiceDao choiceDao;

    @Autowired
    public ChoiceService(ChoiceDao choiceDao) {
        this.choiceDao = choiceDao;
    }

    public void createChoice() {

    }

    public Choice getChoiceByChoiceID(int choice_id) throws ChoiceNotFoundException {
        return Optional.ofNullable(choiceDao.getChoice(choice_id))
                .orElseThrow(ChoiceNotFoundException::new);
    }

    public List<Choice> getChoicesByQuestionID(int question_id) {
        return choiceDao.getChoicesByQuestionID(question_id);
    }

    public void updateChoiceCorrectness(int choice_id, boolean toState) {
        choiceDao.updateChoiceCorrectness(choice_id, toState);
    }

    public void updateChoiceDescription(int choice_id, String description) {
        choiceDao.updateChoiceDescription(choice_id, description);
    }

    public void addChoice(String description, boolean is_correct, int question_id) {
        choiceDao.addChoice(description, is_correct, question_id);
    }
}

