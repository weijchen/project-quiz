package jimmy.myquiz.service;

import jimmy.myquiz.dao.MessageDao;
import jimmy.myquiz.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    private final MessageDao messageDao;

    @Autowired
    public MessageService(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public void sendMessage(String subject, String email, String content) {
        messageDao.sendMessage(subject, email, content);
    }

    public List<Message> getAllMessages() {
        return messageDao.getAllMessages();
    }

    public Message getMessage(int message_id) {
        return messageDao.getMessage(message_id);
    }
}
