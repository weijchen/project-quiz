package jimmy.myquiz.service;

import jimmy.myquiz.dao.CategoryDao;
import jimmy.myquiz.dao.QuestionDao;
import jimmy.myquiz.domain.Category;
import jimmy.myquiz.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {
    private final QuestionDao questionDao;
    private final CategoryDao categoryDao;

    @Autowired
    public QuestionService(QuestionDao questionDao, CategoryDao categoryDao) {
        this.questionDao = questionDao;
        this.categoryDao = categoryDao;
    }

    public List<Question> getAllQuestions() {
        return questionDao.getAllQuestions();
    }

    public List<Category> getAllCategories() {
        return categoryDao.getAllCategories();
    }

    public List<Question> getNQuestionsByCategory(int category_id, int num) {
        List<Question> questions = questionDao.getQuestionsByCategory(category_id);
        questions = questions.stream().filter(Question::isStatus).collect(Collectors.toList());
        Collections.shuffle(questions);
        return questions.subList(0, num);
    }

    public Question getQuestion(int question_id) {
        return questionDao.getQuestion(question_id);
    }

    public String getCategoryName(int category_id) {
        return categoryDao.getCategoryName(category_id);
    }

    public void updateQuestionStatus(int question_id, boolean toStatus) {
        questionDao.updateQuestionStatus(question_id, toStatus);
    }

    public void updateQuestionDescription(int question_id, String description) {
        questionDao.updateQuestion(question_id, description);
    }

    public int addQuestion(String description, int category_id) {
        return questionDao.addQuestion(description, true, category_id);
    }
}
