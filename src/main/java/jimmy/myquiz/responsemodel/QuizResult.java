package jimmy.myquiz.responsemodel;

import jimmy.myquiz.domain.QuestionResponse;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class QuizResult {
    private String name;
    private Date takeDate;
    private Timestamp timeStart;
    private Timestamp timeEnd;
    private int userId;
    private List<QuestionResponse> questionResponses;
    private int passQuestion = 0;
    private int totalQuestion;

    public QuizResult(String name, Date takeDate, Timestamp timeStart, Timestamp timeEnd, int userId, List<QuestionResponse> questionResponses, int totalQuestion) {
        this.name = name;
        this.takeDate = takeDate;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.userId = userId;
        this.questionResponses = questionResponses;
        this.totalQuestion = totalQuestion;
    }

    public void increment() {
        passQuestion++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTakeDate() {
        return takeDate;
    }

    public void setTakeDate(Date takeDate) {
        this.takeDate = takeDate;
    }

    public Timestamp getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Timestamp timeStart) {
        this.timeStart = timeStart;
    }

    public Timestamp getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Timestamp timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<QuestionResponse> getQuestionResponses() {
        return questionResponses;
    }

    public void setQuestionResponses(List<QuestionResponse> questionResponses) {
        this.questionResponses = questionResponses;
    }

    public int getPassQuestion() {
        return passQuestion;
    }

    public void setPassQuestion(int passQuestion) {
        this.passQuestion = passQuestion;
    }

    public int getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        this.totalQuestion = totalQuestion;
    }
}
