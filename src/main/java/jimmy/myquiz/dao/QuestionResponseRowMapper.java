package jimmy.myquiz.dao;

import jimmy.myquiz.domain.QuestionResponse;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class QuestionResponseRowMapper implements RowMapper<QuestionResponse> {
    @Override
    public QuestionResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
        QuestionResponse questionResponse = new QuestionResponse();
        questionResponse.setQuestion_id(rs.getInt("question_id"));
        questionResponse.setQuiz_id(rs.getInt("quiz_id"));
        questionResponse.setUser_choice_id(rs.getInt("user_choice_id"));
        return questionResponse;
    }
}
