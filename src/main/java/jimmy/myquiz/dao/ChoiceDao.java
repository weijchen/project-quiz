package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Choice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChoiceDao {
    JdbcTemplate jdbcTemplate;
    ChoiceRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ChoiceDao(JdbcTemplate jdbcTemplate, ChoiceRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Choice> getChoicesByQuestionID(int question_id) {
        String query = "SELECT * FROM Choice WHERE question_id = :question_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("question_id", question_id);
        List<Choice> choices = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return choices.size() == 0 ? null : choices;
    }

    public Choice getChoice(int choice_id) {
        String query = "SELECT * FROM Choice WHERE choice_id = :choice_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("choice_id", choice_id);
        List<Choice> choices = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return choices.size() == 0 ? null : choices.get(0);
    }

    public void updateChoiceDescription(int choice_id, String description) {
        String query = "UPDATE Choice SET description = :description WHERE choice_id = :choice_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("description", description);
        sqlParameterSource.addValue("choice_id", choice_id);
        namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }

    public void updateChoiceCorrectness(int choice_id, boolean toState) {
        String query = "UPDATE Choice SET is_correct = :is_correct WHERE choice_id = :choice_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("is_correct", toState);
        sqlParameterSource.addValue("choice_id", choice_id);
        namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }

    public int addChoice(String description, boolean is_correct, int question_id) {
        String query = "INSERT INTO Choice (description, is_correct, question_id) VALUES (:description, :is_correct, :question_id)";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("description", description);
        sqlParameterSource.addValue("is_correct", is_correct);
        sqlParameterSource.addValue("question_id", question_id);
        return namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }

    public boolean isChoiceIdValid(Integer id) {
        String query = "SELECT * FROM Choice WHERE choice_id = :choice_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("choice_id", id);
        List<Choice> choices = namedParameterJdbcTemplate.query(query, sqlParameterSource, rowMapper);
        return choices.size() == 0;
    }
}
