package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryDao {
    JdbcTemplate jdbcTemplate;
    CategoryRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public CategoryDao(JdbcTemplate jdbcTemplate, CategoryRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Category> getAllCategories() {
        String query = "SELECT * FROM Category";
        return jdbcTemplate.query(query, rowMapper);
    }

    public String getCategoryName(int category_id) {
        String query = "SELECT * FROM Category WHERE category_id = :category_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("category_id", category_id);
        List<Category> categories = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return categories.size() == 0 ? "" : categories.get(0).getName();
    }
}
