package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Quiz;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class QuizRowMapper implements RowMapper<Quiz> {

    @Override
    public Quiz mapRow(ResultSet rs, int rowNum) throws SQLException {
        Quiz quiz = new Quiz();
        quiz.setQuiz_id(rs.getInt("quiz_id"));
        quiz.setName(rs.getString("name"));
        quiz.setTake_date(rs.getDate("take_date"));
        quiz.setUser_id(rs.getInt("user_id"));
        quiz.setTime_start(rs.getTimestamp("time_start"));
        quiz.setTime_end(rs.getTimestamp("time_end"));
        quiz.setCategory_id(rs.getInt("category_id"));
        return quiz;
    }
}
