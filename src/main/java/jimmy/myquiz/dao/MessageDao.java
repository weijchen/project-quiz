package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class MessageDao {
    JdbcTemplate jdbcTemplate;
    MessageRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public MessageDao(JdbcTemplate jdbcTemplate, MessageRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Message> getAllMessages() {
        String query = "SELECT * FROM Message";
        return jdbcTemplate.query(query, rowMapper);
    }

    public Message getMessage(int message_id) {
        String query = "SELECT * FROM Message WHERE message_id = :message_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("message_id", message_id);
        List<Message> messages = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return messages.size() == 0 ? null : messages.get(0);
    }

    public List<Message> getMessageByEmail(String email) {
        String query = "SELECT * FROM Message WHERE email = :email";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("email", email);
        List<Message> messages = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return messages;
    }

    public void sendMessage(String subject, String email, String content) {
        String query = "INSERT INTO Message (subject, email, content, create_time) VALUES (:subject, :email, :content, :create_time)";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("subject", subject);
        mapSqlParameterSource.addValue("email", email);
        mapSqlParameterSource.addValue("content", content);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        mapSqlParameterSource.addValue("create_time", dtf.format(now));
        namedParameterJdbcTemplate.update(query, mapSqlParameterSource);
    }
}
