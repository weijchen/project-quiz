package jimmy.myquiz.dao;

import jimmy.myquiz.domain.QuestionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QuestionResponseDao {
    JdbcTemplate jdbcTemplate;
    QuestionResponseRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public QuestionResponseDao(JdbcTemplate jdbcTemplate, QuestionResponseRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public QuestionResponse getQuestionResponse(int question_id, int quiz_id) {
        String query = "SELECT * FROM QuestionResponse WHERE question_id = :question_id AND quiz_id = :quiz_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("question_id", question_id);
        sqlParameterSource.addValue("quiz_id", quiz_id);
        List<QuestionResponse> responses = namedParameterJdbcTemplate.query(query, sqlParameterSource, rowMapper);
        return responses.size() == 0 ? null : responses.get(0);
    }

    public List<QuestionResponse> getQuestionsResponsesByQuizId(int quiz_id) {
        String query = "SELECT * FROM QuestionResponse WHERE quiz_id = :quiz_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("quiz_id", quiz_id);
        List<QuestionResponse> responses = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return responses.size() == 0 ? new ArrayList<>() : responses;
    }

    public void createQuestionResponse(int question_id, int quiz_id, int user_choice_id) {
        String query = "INSERT INTO QuestionResponse (question_id, quiz_id, user_choice_id) VALUES (:question_id, :quiz_id, :user_choice_id)";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("question_id", question_id);
        mapSqlParameterSource.addValue("quiz_id", quiz_id);
        mapSqlParameterSource.addValue("user_choice_id", user_choice_id);
        namedParameterJdbcTemplate.update(query, mapSqlParameterSource);
    }

    public void updateQuestionResponse(int question_id, int quiz_id, int user_choice_id) {
        String query = "UPDATE QuestionResponse SET user_choice_id = :user_choice_id WHERE question_id = :question_id AND quiz_id = :quiz_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("question_id", question_id);
        sqlParameterSource.addValue("quiz_id", quiz_id);
        sqlParameterSource.addValue("user_choice_id", user_choice_id);
        namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }
}
