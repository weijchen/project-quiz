package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class QuestionDao {
    JdbcTemplate jdbcTemplate;
    QuestionRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public QuestionDao(JdbcTemplate jdbcTemplate, QuestionRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Question> getAllQuestions() {
        String query = "SELECT * FROM Question";
        return jdbcTemplate.query(query, rowMapper);
    }

    public List<Question> getQuestionsByCategory(int category_id) {
        String query = "SELECT * FROM Question WHERE category_id = :category_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("category_id", category_id);
        List<Question> questions = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return questions;
    }

    public Question getQuestion(int question_id) {
        String query = "SELECT * FROM Question WHERE question_id = :question_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("question_id", question_id);
        List<Question> questions = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return questions.size() == 0 ? null : questions.get(0);
    }

    public void updateQuestionStatus(int question_id, boolean toState) {
        String query = "UPDATE Question SET status = :status WHERE question_id = :question_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("status", toState);
        sqlParameterSource.addValue("question_id", question_id);
        namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }

    public void updateQuestion(int question_id, String description) {
        String query = "UPDATE Question SET description = :description WHERE question_id = :question_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("description", description);
        sqlParameterSource.addValue("question_id", question_id);
        namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }

    public int addQuestion(String description, boolean status, int category_id) {
        String query = "INSERT INTO Question (description, status, category_id) VALUES (:description, :status, :category_id)";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("description", description);
        sqlParameterSource.addValue("status", status);
        sqlParameterSource.addValue("category_id", category_id);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(query, sqlParameterSource, keyHolder);
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }
}
