package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
public class QuizDao {
    JdbcTemplate jdbcTemplate;
    QuizRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public QuizDao(JdbcTemplate jdbcTemplate, QuizRowMapper rowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<Quiz> getQuizzes() {
        String query = "SELECT * FROM Quiz ORDER BY take_date DESC";
        return jdbcTemplate.query(query, rowMapper);
    }

    public List<Quiz> getQuizzes(String contents) {
        String query = "SELECT * FROM Quiz JOIN User ON Quiz.user_id = User.user_id WHERE User.email LIKE :contents OR User.firstname LIKE :contents OR User.lastname LIKE :contents ORDER BY Quiz.take_date DESC";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("contents", "%" + contents + "%");
        List<Quiz> quizzes = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return quizzes.size() == 0 ? new ArrayList<>() : quizzes;
    }

    public List<Quiz> getQuizzesByUserId(int user_id) {
        String query = "SELECT * FROM Quiz WHERE user_id = :user_id ORDER BY take_date DESC";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("user_id", user_id);
        List<Quiz> quizzes = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return quizzes.size() == 0 ? null : quizzes;
    }

    public Quiz getQuizByQuizId(int quiz_id) {
        String query = "SELECT * FROM Quiz WHERE quiz_id = :quiz_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("quiz_id", quiz_id);
        List<Quiz> quiz = namedParameterJdbcTemplate.query(query, mapSqlParameterSource, rowMapper);
        return quiz.size() == 0 ? null : quiz.get(0);
    }

    public int createQuiz(String name, int user_id, int category_id) {
        String query = "INSERT INTO Quiz (name, take_date, time_start, time_end, user_id, category_id) VALUES (:name, :take_date, :time_start, :time_end, :user_id, :category_id)";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("name", name);
        mapSqlParameterSource.addValue("take_date", new Date(System.currentTimeMillis()));
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        mapSqlParameterSource.addValue("time_start", dtf.format(now));
        mapSqlParameterSource.addValue("time_end", dtf.format(now));
        mapSqlParameterSource.addValue("user_id", user_id);
        mapSqlParameterSource.addValue("category_id", category_id);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(query, mapSqlParameterSource, keyHolder);
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    public void updateQuizEndTime(int quiz_id, LocalDateTime time_end) {
        String query = "UPDATE Quiz SET time_end = :time_end WHERE quiz_id = :quiz_id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("quiz_id", quiz_id);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        mapSqlParameterSource.addValue("time_end", dtf.format(time_end));
        namedParameterJdbcTemplate.update(query, mapSqlParameterSource);
    }
}
