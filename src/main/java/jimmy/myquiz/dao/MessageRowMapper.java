package jimmy.myquiz.dao;

import jimmy.myquiz.domain.Message;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MessageRowMapper implements RowMapper<Message> {
    @Override
    public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
        Message message = new Message();
        message.setMessage_id(rs.getInt("message_id"));
        message.setContent(rs.getString("content"));
        message.setEmail(rs.getString("email"));
        message.setSubject(rs.getString("subject"));
        message.setCreate_time(rs.getTimestamp("create_time"));
        return message;
    }
}
