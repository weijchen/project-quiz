package jimmy.myquiz.dao;

import jimmy.myquiz.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDao {
    JdbcTemplate jdbcTemplate;
    UserRowMapper rowMapper;
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UserDao(JdbcTemplate jdbcTemplate, UserRowMapper userRowMapper, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = userRowMapper;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<User> getUsers() {
        String query = "SELECT * FROM User";
        return jdbcTemplate.query(query, rowMapper);
    }

    public boolean isUserExist(String email) {
        String query = "SELECT * FROM User WHERE email = :email";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("email", email);
        List<User> user = namedParameterJdbcTemplate.query(query, parameterSource, rowMapper);

        return user.size() != 0;
    }

    public boolean isUserExist(int user_id) {
        String query = "SELECT * FROM User WHERE user_id = :user_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("user_id", user_id);
        List<User> user = namedParameterJdbcTemplate.query(query, parameterSource, rowMapper);

        return user.size() != 0;
    }

    public void createUser(String firstname, String lastname, String email, String password, boolean is_admin) {
        String query = "INSERT INTO User (firstname, lastname, email, password, is_active, is_admin) VALUES (:firstname, :lastname, :email, :password, :is_active, :is_admin)";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("firstname", firstname);
        mapSqlParameterSource.addValue("lastname", lastname);
        mapSqlParameterSource.addValue("email", email);
        mapSqlParameterSource.addValue("password", password);
        mapSqlParameterSource.addValue("is_active", true);
        mapSqlParameterSource.addValue("is_admin", is_admin);
        namedParameterJdbcTemplate.update(query, mapSqlParameterSource);
    }

    public User getUser(int user_id) {
        String query = "SELECT * FROM User WHERE user_id = :user_id";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("user_id", user_id);
        List<User> user = namedParameterJdbcTemplate.query(query, parameterSource, rowMapper);

        return user.size() == 0 ? null : user.get(0);
    }

    public int updateUserStatus(int user_id, boolean toState) {
        String query = "UPDATE User SET is_active = :is_active WHERE user_id = :user_id";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("is_active", toState);
        sqlParameterSource.addValue("user_id", user_id);
        return namedParameterJdbcTemplate.update(query, sqlParameterSource);
    }
}
