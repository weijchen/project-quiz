package jimmy.myquiz.exception;

public class ChoiceNotFoundException extends Exception{
    public ChoiceNotFoundException() {
        super("Question not found");
    }
}
