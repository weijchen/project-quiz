package jimmy.myquiz.controller;

import jimmy.myquiz.domain.Message;
import jimmy.myquiz.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ContactController {
    private MessageService messageService;

    @Autowired
    public ContactController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/contact")
    public String getContact(Model model) {
        return "contact";
    }

    @PostMapping("/contact")
    public String sendContact(@RequestParam String subject,
                              @RequestParam String email,
                              @RequestParam String content,
                              Model model) {
        messageService.sendMessage(subject, email, content);
        return "redirect:/contact";
    }

    @GetMapping("/contacts")
    public String getContacts(HttpServletRequest request,
                              Model model) {
        HttpSession session = request.getSession();
        if (!(boolean) session.getAttribute("admin")) return "redirect:/home";

        List<Message> messageList = messageService.getAllMessages();
        session.setAttribute("messages", messageList);
        return "contacts";
    }

    @GetMapping("/contacts/view")
    public String getContactByID(@RequestParam("cid") int cid,
                                 HttpServletRequest request,
                                 Model model) {
        Message message = messageService.getMessage(cid);
        HttpSession session = request.getSession();
        session.setAttribute("viewMessage", message);
        return "contact-view";
    }
}
