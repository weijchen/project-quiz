package jimmy.myquiz.controller;

import jimmy.myquiz.domain.Choice;
import jimmy.myquiz.domain.Question;
import jimmy.myquiz.domain.QuestionResponse;
import jimmy.myquiz.exception.ChoiceNotFoundException;
import jimmy.myquiz.responsemodel.QuizResult;
import jimmy.myquiz.service.ChoiceService;
import jimmy.myquiz.service.QuestionService;
import jimmy.myquiz.service.QuizService;
import jimmy.myquiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class QuizResultController {
    private QuizService quizService;
    private UserService userService;
    private QuestionService questionService;
    private ChoiceService choiceService;
    private final static int NUM_QUESTION = 5;

    @Autowired
    public QuizResultController(QuizService quizService, UserService userService, QuestionService questionService, ChoiceService choiceService) {
        this.quizService = quizService;
        this.userService = userService;
        this.questionService = questionService;
        this.choiceService = choiceService;
    }

    @GetMapping("quiz-result")
    public String getQuizResult(@RequestParam("q") int quiz_id,
                                @RequestParam("u") int user_id,
                                HttpServletRequest request,
                                Model model) throws ChoiceNotFoundException {
        QuizResult quizResult = quizService.getQuizResult(quiz_id);
        List<QuestionResponse> questionResponses = quizResult.getQuestionResponses();
        String fullName = userService.getUserFullName(user_id);
        List<Question> questions = new ArrayList<>();
        List<List<Choice>> choices = new ArrayList<>();
        List<Integer> selections = new ArrayList<>();

        for (int i = 0; i < questionResponses.size(); i++) {
            int question_id = questionResponses.get(i).getQuestion_id();
            questions.add(questionService.getQuestion(question_id));
            List<Choice> choicesForQuestion = choiceService.getChoicesByQuestionID(question_id);
            choices.add(choicesForQuestion);
            selections.add(questionResponses.get(i).getUser_choice_id());
        }

        HttpSession session = request.getSession();
        session.setAttribute("quizResult", quizResult);
        session.setAttribute("fullName", fullName);
        session.setAttribute("qsize", NUM_QUESTION);
        session.setAttribute("questions", questions);
        session.setAttribute("choices", choices);
        session.setAttribute("selections", selections);
        return "quiz-result";
    }
}
