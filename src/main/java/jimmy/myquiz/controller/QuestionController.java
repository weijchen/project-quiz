package jimmy.myquiz.controller;

import jimmy.myquiz.domain.Category;
import jimmy.myquiz.domain.Choice;
import jimmy.myquiz.domain.Question;
import jimmy.myquiz.exception.ChoiceNotFoundException;
import jimmy.myquiz.service.ChoiceService;
import jimmy.myquiz.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class QuestionController {
    private QuestionService questionService;
    private ChoiceService choiceService;

    @Autowired
    public QuestionController(QuestionService questionService, ChoiceService choiceService) {
        this.questionService = questionService;
        this.choiceService = choiceService;
    }

    @GetMapping("/questions")
    public String getQuestions(HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        List<Question> questions = questionService.getAllQuestions();
        List<String> categories = new ArrayList<>();

        for (Question question : questions) {
            categories.add(questionService.getCategoryName(question.getCategory_id()));
        }

        session.setAttribute("numQuestion", questions.size());
        session.setAttribute("questions", questions);
        session.setAttribute("categories", categories);
        return "questions";
    }

    @PostMapping("/question-update")
    public String updateUser(@RequestParam("toStatus") boolean toStatus,
                             @RequestParam("question_id") int question_id,
                             HttpServletRequest request,
                             Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        questionService.updateQuestionStatus(question_id, toStatus);
        return "redirect:/questions";
    }

    @GetMapping("/question/add")
    public String getQuestionAdd(HttpServletRequest request,
                                 Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        List<Category> categories = questionService.getAllCategories();
        System.out.println("here" + categories.size());
        session.setAttribute("categories", categories);
        return "question-add";
    }

    @PostMapping("/question/add")
    public String getQuestionAdd(@RequestParam String description,
                                 @RequestParam int category,
                                 @RequestParam String descOne,
                                 @RequestParam String descTwo,
                                 @RequestParam String descThree,
                                 @RequestParam String descFour,
                                 @RequestParam int answer,
                                 HttpServletRequest request,
                                 Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        int question_id = questionService.addQuestion(description, category);
        choiceService.addChoice(descOne, answer == 1, question_id);
        choiceService.addChoice(descTwo, answer == 2, question_id);
        choiceService.addChoice(descThree, answer == 3, question_id);
        choiceService.addChoice(descFour, answer == 4, question_id);
        return "redirect:/questions";
    }

    @GetMapping("/question/edit")
    public String getQuestionEdit(@RequestParam(value = "question_id") int question_id,
                                  HttpServletRequest request,
                                  Model model) throws ChoiceNotFoundException {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        Question question = questionService.getQuestion(question_id);
        String category = questionService.getCategoryName(question.getCategory_id());
        List<Choice> choices = choiceService.getChoicesByQuestionID(question_id);

        session.setAttribute("question", question);
        session.setAttribute("category", category);
        session.setAttribute("numChoice", choices.size());
        session.setAttribute("choices", choices);
        return "question-edit";
    }

    @PostMapping("/question/edit/question")
    public String editQuestion(@RequestParam("question_id") int question_id,
                               @RequestParam("description") String description,
                               HttpServletRequest request,
                               Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        questionService.updateQuestionDescription(question_id, description.substring(1, description.length()));
        return "redirect:/question/edit?question_id=" + question_id;
    }

    @PostMapping("/question/edit/choice-description")
    public String editChoice(@RequestParam("question_id") int question_id,
                             @RequestParam("description") String description,
                             @RequestParam("choice_id") int choice_id,
                             HttpServletRequest request,
                             Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        choiceService.updateChoiceDescription(choice_id, description.substring(1, description.length()));
        return "redirect:/question/edit?question_id=" + question_id;
    }

    @PostMapping("/question/edit/choice-answer")
    public String editChoice(@RequestParam("question_id") int question_id,
                             @RequestParam("choice_id") int choice_id,
                             HttpServletRequest request,
                             Model model) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        List<Choice> choices = choiceService.getChoicesByQuestionID(question_id);
        for (Choice choice : choices) {
            choiceService.updateChoiceCorrectness(choice.getChoice_id(), false);
        }
        choiceService.updateChoiceCorrectness(choice_id, true);
        return "redirect:/question/edit?question_id=" + question_id;
    }
}
