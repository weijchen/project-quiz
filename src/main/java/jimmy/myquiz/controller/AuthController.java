package jimmy.myquiz.controller;

import jimmy.myquiz.domain.User;
import jimmy.myquiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class AuthController {
    private UserService userService;
    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping("/register")
    public String getRegister(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("errorMsg", "");
        return "register";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Model model) {
        HttpSession oldSession = request.getSession(false);
        // invalidate old session if it exists
        if(oldSession != null) oldSession.invalidate();
        return "redirect:/login";
    }

    @PostMapping("/login")
    public String login(@RequestParam String email,
                        @RequestParam String password,
                        HttpServletRequest request) {
        Optional<User> possibleUser = userService.validateLogin(email, password);

        if (possibleUser.isPresent()) {
            HttpSession oldSession = request.getSession(false);
            // invalidate old session if it exists
            if (oldSession != null) oldSession.invalidate();

            // generate new session
            HttpSession newSession = request.getSession(true);

            // store user details in session
            newSession.setAttribute("email", possibleUser.get().getEmail());
            newSession.setAttribute("uid", possibleUser.get().getUser_id());
            newSession.setAttribute("firstname", possibleUser.get().getFirstname());
            newSession.setAttribute("lastname", possibleUser.get().getLastname());
            newSession.setAttribute("admin", possibleUser.get().is_admin());

            return "redirect:/home";
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("errorMsg", "Credential Error or the Account has been deactivated");
            return "login";
        }
    }

    @PostMapping("/register")
    public String register(@RequestParam String firstname,
                           @RequestParam String lastname,
                           @RequestParam String email,
                           @RequestParam String password,
                           Model model) {
        if (userService.isUserExist(email)) {
            model.addAttribute("errorMsg", "Email registered, please use another email address.");
            return "register";
        }

        userService.createUser(firstname, lastname, email, password, false);
        return "redirect:/login";
    }
}
