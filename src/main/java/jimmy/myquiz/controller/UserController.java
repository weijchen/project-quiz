package jimmy.myquiz.controller;

import jimmy.myquiz.domain.User;
import jimmy.myquiz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String getUsers(HttpServletRequest request,
                           Model model) {
        HttpSession session = request.getSession();

        List<User> users = userService.getUsersNotAdmin();
        session.setAttribute("users", users);
        return "users";
    }

    @PostMapping("/user-update")
    public String updateUser(@RequestParam("toStatus") boolean toStatus,
                             @RequestParam("user_id") int user_id,
                             HttpServletRequest request,
                             Model model) {
        HttpSession session = request.getSession();
        return "redirect:/users";
    }
}
