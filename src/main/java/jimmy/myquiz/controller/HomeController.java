package jimmy.myquiz.controller;

import jimmy.myquiz.domain.Category;
import jimmy.myquiz.domain.Quiz;
import jimmy.myquiz.service.QuestionService;
import jimmy.myquiz.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class HomeController {

    private QuestionService questionService;
    private QuizService quizService;

    @Autowired
    public HomeController(QuestionService questionService, QuizService quizService) {
        this.questionService = questionService;
        this.quizService = quizService;
    }

    @GetMapping("/home")
    public String getHome(Model model, HttpServletRequest request) {
        List<Category> categories = questionService.getAllCategories();
        model.addAttribute("categories", categories);

        HttpSession session = request.getSession();
        int id = (int) session.getAttribute("uid");
        List<Quiz> quizzes = quizService.getQuizzesByUserId(id);
        model.addAttribute("quizzes", quizzes);

        return "home";
    }
}
