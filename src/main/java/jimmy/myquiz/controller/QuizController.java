package jimmy.myquiz.controller;

import jimmy.myquiz.domain.Choice;
import jimmy.myquiz.domain.Question;
import jimmy.myquiz.domain.Quiz;
import jimmy.myquiz.responsemodel.QuizResult;
import jimmy.myquiz.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class QuizController {
    private QuizService quizService;
    private QuestionService questionService;
    private ChoiceService choiceService;
    private QuestionResponseService questionResponseService;
    private UserService userService;
    private final static int NUM_QUESTION = 5;

    @Autowired
    public QuizController(QuizService quizService, QuestionService questionService, ChoiceService choiceService, QuestionResponseService questionResponseService, UserService userService) {
        this.quizService = quizService;
        this.questionService = questionService;
        this.choiceService = choiceService;
        this.questionResponseService = questionResponseService;
        this.userService = userService;
    }

    @GetMapping("/quiz")
    public String getQuiz(@RequestParam(value = "cid", required = false) Integer category_id,
                          @RequestParam(value = "cur_q", required = false) Integer cur_q,
                          HttpServletRequest request,
                          Model model) {
        HttpSession session = request.getSession(false);
        List<Question> questions;
        int cur;

        if (session.getAttribute("ongoing") == null || !(boolean) session.getAttribute("ongoing")) {
            session.setAttribute("hasFinish", false);
            questions = createQuiz(category_id, request);
            cur = 1;
            session.setAttribute("cur", cur);
        } else {
            questions = (List<Question>) session.getAttribute("ongoingQuestions");
            if (cur_q != null) session.setAttribute("cur", cur_q);
            else session.setAttribute("cur", 1);
        }

        List<List<Choice>> choices = new ArrayList<>();
        List<Integer> selections = new ArrayList<>();
        int qid = (int) session.getAttribute("qid");


        for (int i = 0; i < questions.size(); i++) {
            int question_id = questions.get(i).getQuestion_id();
            List<Choice> choicesForQuestion = choiceService.getChoicesByQuestionID(question_id);
            choices.add(choicesForQuestion);
            selections.add(questionResponseService.getQuestionResponse(question_id, qid));
        }

        session.setAttribute("ongoingQuestions", questions);
        session.setAttribute("qsize", NUM_QUESTION);
        session.setAttribute("choices", choices);
        session.setAttribute("selections", selections);
        session.setAttribute("ongoing", true);
        session.setAttribute("hasFinish", true);

//        Check whether all questions are done
        for (int selection : selections) {
            if (selection == -1) session.setAttribute("hasFinish", false);
        }
        return "quiz";
    }

    public List<Question> createQuiz(int category_id, HttpServletRequest request) {
        List<Question> questions = questionService.getNQuestionsByCategory(category_id, NUM_QUESTION);

        HttpSession session = request.getSession();
        int id = (int) session.getAttribute("uid");
        String firstname = (String) session.getAttribute("firstname");
        String lastname = (String) session.getAttribute("lastname");
        int qid = quizService.createQuiz(category_id, firstname + "_" + lastname, id);
        session.setAttribute("qid", qid);
        return questions;
    }

    @PostMapping("/quiz/update-cur")
    public String updateCur(@RequestParam("cur") int cur,
                            HttpServletRequest request,
                            Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("cur", cur);
        return "redirect:/quiz?cur_q=" + cur;
    }

    @PostMapping("/quiz")
    public String chooseAnswer(@RequestParam("question_id") int question_id,
                               @RequestParam("user_choice_id") int user_choice_id,
                               @RequestParam("cur") int cur,
                               HttpServletRequest request,
                               Model model) {
        HttpSession session = request.getSession();
        int qid = (int) session.getAttribute("qid");
        questionResponseService.updateQuestionResponse(question_id, qid, user_choice_id);
        return "redirect:/quiz?cur_q=" + cur;
    }

    @PostMapping("/submit-quiz")
    public String quizSubmit(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int qid = (int) session.getAttribute("qid");
        quizService.updateQuiz(qid, LocalDateTime.now());
        session.removeAttribute("ongoing");
        return "redirect:/home";
    }

    @GetMapping("/quizzes")
    public String getQuizzes(@RequestParam(value = "category", required = false, defaultValue = "") String category,
                             @RequestParam(value = "userInfo", required = false, defaultValue = "") String userInfo,
                             HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (!(boolean)session.getAttribute("admin")) return "redirect:/home";

        List<Quiz> quizzes = quizService.getQuizzes(category, userInfo);
        List<String> quizCategory = new ArrayList<>();
        List<String> usersFullName = new ArrayList<>();
        List<Integer> passedQuestion = new ArrayList<>();
        List<Integer> totalQuestion = new ArrayList<>();

        for (Quiz quiz : quizzes) {
            QuizResult result = quizService.getQuizResult(quiz.getQuiz_id());
            quizCategory.add(questionService.getCategoryName(quiz.getCategory_id()));
            usersFullName.add(userService.getUserFullName(quiz.getUser_id()));
            passedQuestion.add(result.getPassQuestion());
            totalQuestion.add(result.getTotalQuestion());
        }

        session.setAttribute("numQuizzes", quizzes.size());
        session.setAttribute("quizzes", quizzes);
        session.setAttribute("categories", quizCategory);
        session.setAttribute("users", usersFullName);
        session.setAttribute("passed", passedQuestion);
        session.setAttribute("total", totalQuestion);
        session.setAttribute("searchCategory", category);
        session.setAttribute("searchUserInfo", userInfo);
        return "quizzes";
    }
}
