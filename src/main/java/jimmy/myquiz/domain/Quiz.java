package jimmy.myquiz.domain;

import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Quiz {
    private int quiz_id;
    private String name;
    private Date take_date;
    private Timestamp time_start;
    private Timestamp time_end;
    private int user_id;
    private int category_id;
}
