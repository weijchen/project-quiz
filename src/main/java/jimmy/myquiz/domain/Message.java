package jimmy.myquiz.domain;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Message {
    private int message_id;
    private String subject;
    private String email;
    private String content;
    private Timestamp create_time;
}
