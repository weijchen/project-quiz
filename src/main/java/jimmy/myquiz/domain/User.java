package jimmy.myquiz.domain;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private int user_id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    @Getter(AccessLevel.NONE)
    private boolean is_active;
    private boolean is_admin;

    public boolean getIs_active() {
        return is_active;
    }
}
