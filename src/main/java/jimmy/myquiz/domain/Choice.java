package jimmy.myquiz.domain;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Choice {
    private int choice_id;
    private String description;
    @Getter(AccessLevel.NONE)
    private boolean is_correct;
    private int question_id;

    public boolean getIs_correct() {
        return is_correct;
    }
}
