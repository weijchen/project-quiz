package jimmy.myquiz.domain;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Question {
    private int question_id;
    private String description;
    private boolean status;
    private int category_id;
    private List<Choice> choices;
}
