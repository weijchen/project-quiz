package jimmy.myquiz.domain;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QuestionResponse {
    private int question_id;
    private int quiz_id;
    private int user_choice_id;
}
