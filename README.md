# project-quiz

A quizlet like application build with Spring Framework and jsp files

## Project Features
### General
- Authentication (Login / Register)
- Authorization: separate pages based on the identity of login user
- Contact us page

### Admin User
- User Management
  - Activate/Deactivate
- Question Management
  - Create
  - Update (Contents, Status)
  - Delete

### Normal User
- Question service
  - Answer questions in a quiz
  - Have the ability to view previous results

### Screenshots
- Login Page
![Login Page](./login.png)

- Home Page
![Home Page](./home.png)

- Answer Question
![Answer Question](./answer-question.png)

- User Management
![User Management](./user-management.png)

- Question Management
![Question Management](./question-management.png)
